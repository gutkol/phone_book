package com.common;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringBootstrap {

	private static ApplicationContext context;
	
	public static ApplicationContext buildContext(){
		if(context == null){
			context = new ClassPathXmlApplicationContext("file:src/main/resources/appContext.xml");
		}
		return context;
	}
	
	public static ApplicationContext getContext() throws NullPointerException {
		if(context == null){
			throw new NullPointerException("Context is null. Should use buildContext method before usage.");
		}
		return context;
	}
}
