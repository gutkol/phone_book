package com.common;

public abstract interface CustomFunction {

	abstract <T> Boolean run(T first, T second);
}
