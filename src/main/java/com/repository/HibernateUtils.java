package com.repository;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtils {
	private static final SessionFactory ourSessionFactory;
	private static final StandardServiceRegistry  serviceRegistry;

	static {
		try {
			serviceRegistry = new StandardServiceRegistryBuilder()
					.configure()
					.build();
			
			ourSessionFactory = new MetadataSources( serviceRegistry )
					.buildMetadata()
					.buildSessionFactory();
			
		} catch (Throwable ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() throws HibernateException {
		return ourSessionFactory;
	}
}
