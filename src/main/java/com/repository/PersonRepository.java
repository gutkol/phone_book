package com.repository;

import java.util.Collection;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.springframework.stereotype.Repository;

import com.models.Person;

@Repository("personRepository")
public class PersonRepository implements IRepository<Person> {

	public int Add(Person entity) {	
		Session session = getSession();
		int result = 0;
		final Transaction transaction = session.beginTransaction();
		try {
			result = (Integer) session.save(entity);
			Hibernate.initialize(result);
			transaction.commit();
		} catch (Exception ex) {
		} finally {
			if (transaction.getStatus() == TransactionStatus.ACTIVE) {
				try {
					transaction.rollback();
				} catch (Exception e) {
				}
			}
			try {
				session.close();
			} catch (Exception e) {
			}
			return result;
		}
	}

	public void Remove(Person entity) {
		Session session = getSession();
		final Transaction transaction = session.beginTransaction();
		try {
			session.delete(entity);
			transaction.commit();
		} finally {
			if (transaction.getStatus() == TransactionStatus.ACTIVE) {
				try {
					transaction.rollback();
				} catch (Exception e) {
				}
			}
			try {
				session.close();
			} catch (Exception e) {
			}
		}
		
	}

	public void Remove(int id) {
		Session session = getSession();
		final Transaction transaction = session.beginTransaction();
		try {
			session.delete(id);
			transaction.commit();
		} finally {
			if (transaction.getStatus() == TransactionStatus.ACTIVE) {
				try {
					transaction.rollback();
				} catch (Exception e) {
				}
			}
			try {
				session.close();
			} catch (Exception e) {
			}
		}
	}

	public Collection<Person> Get() {
		Session session = getSession();
		Collection<Person> result = null;
		final Transaction transaction = session.beginTransaction();
		try {
			result = (Collection<Person>) session.createCriteria(Person.class).list();
			Hibernate.initialize(result);
			transaction.commit();
		} catch (Exception ex) {
			
		} finally {
			if (transaction.getStatus() == TransactionStatus.ACTIVE) {
				try {
					transaction.rollback();
				} catch (Exception e) {
				}
			}
			try {
				session.close();
			} catch (Exception e) {
			}
			return result;
		}
	}
	
	private Session getSession() {
		return HibernateUtils.getSessionFactory().openSession();
	}
}
