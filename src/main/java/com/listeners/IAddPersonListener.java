package com.listeners;

import com.models.Person;

public interface IAddPersonListener {

	public void added(Person person);
}
