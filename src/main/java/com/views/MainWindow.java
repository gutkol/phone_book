package com.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.services.IDialogManager;

@Component("mainWindow")
public class MainWindow extends JFrame {
	
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem importItem;
	private JMenuItem exportItem;
	private JMenuItem closeItem;
	
	private PersonTablePanel personTablePanel;
	private IDialogManager dialogManager;

	@Autowired
	public MainWindow(
			PersonTablePanel personTablePanel,
			IDialogManager dialogManager) {
		
		this.personTablePanel = personTablePanel;
		this.dialogManager = dialogManager;
		
		setTitle("Phone Book");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		int width = 800;
		int height = 800;

		setSize(width, height);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screen.width - width) / 2;
		int y = (screen.height - height) / 2;
		setBounds(x, y, width, height);

		setLayout(new BorderLayout());

		menuBar = new JMenuBar();
		menu = new JMenu("File");
		importItem = new JMenuItem("Import to XML");
		importItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
			    File file =dialogManager.showOpenDialog(getRootPane());
			    if(file != null){
			    	try {
						personTablePanel.Import(file);
					} catch (JAXBException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			    }
			}
		});
		menu.add(importItem);
		exportItem = new JMenuItem("Export to XML");
		exportItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String path = dialogManager.showSaveDialog(getRootPane());
				if(path != null){
					try {
						personTablePanel.export(path);
					} catch (JAXBException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		menu.add(exportItem);
		menu.addSeparator();
		closeItem = new JMenuItem("Close");
		closeItem.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				System.exit(0);
			}
		});
		menu.add(closeItem);
		menuBar.add(menu);
		add(menuBar, BorderLayout.NORTH);
		add(personTablePanel, BorderLayout.CENTER);
	}
}
