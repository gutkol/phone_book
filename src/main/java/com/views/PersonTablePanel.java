package com.views;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.listeners.IAddPersonListener;
import com.models.Person;
import com.services.IPersonService;

@Component("personTablePanel")
public class PersonTablePanel extends JPanel implements IAddPersonListener {

	private JTable table;
	private TableModel tableModel;
	private JPanel panel;
	private JButton addButton;
	private JButton deleteButton;
	
	private Box searchPanel;
	private JTextField searchTextField;
	private JButton searchButton;
	
	private AddWindow addWindow;
	
	private IPersonService personService;
	
	@Autowired
	private PersonTablePanel(
			IPersonService personService) {
		
		this.personService = personService;
		
		setLayout(new BorderLayout());
	
		this.tableModel = personService.getTableModel();
		table = new JTable(tableModel);
		
		InitializeButtons(personService);
		searchTextField = new JTextField();
		searchPanel = Box.createHorizontalBox();
		searchPanel.add(searchTextField);
		searchPanel.add(searchButton);
		add(searchPanel,BorderLayout.NORTH);
		add(new JScrollPane(table), BorderLayout.CENTER);
		panel = new JPanel();
		panel.add(addButton, BorderLayout.EAST);
		panel.add(deleteButton, BorderLayout.WEST);
		add(panel, BorderLayout.SOUTH);
		personService.getPersons();
		
	}

	private void InitializeButtons(IPersonService personService) {
		addButton = new JButton("Add");
		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				InitializeAddWindow();
				addWindow.setVisible(true);
			}
		});
		deleteButton = new JButton("Delete");
		deleteButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Boolean wasSuccesful = personService.tryDelete(table.getSelectedRow());
				if(!wasSuccesful){
					JOptionPane.showMessageDialog(addButton,
						    "Can not delete person to data base",
						    "Add",
						    JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		
		searchButton = new JButton("Search");
		searchButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				personService.search(searchTextField.getText());
			}
		});
	}
	
	private void InitializeAddWindow() {
		addWindow = new AddWindow();
		addWindow.addActionListener(this);
	}

	@Override
	public void added(Person person) {
		Boolean wasSuccesful = personService.tryAdd(person);
		if(!wasSuccesful){
			JOptionPane.showMessageDialog(addButton,
				    "Can not add person to data base",
				    "Add",
				    JOptionPane.WARNING_MESSAGE);
		}
	}
	
	public void export(String path) throws JAXBException{
		personService.export(path);
	}
	
	public void Import(File file) throws JAXBException{
		personService.Import(file);
	}
}
