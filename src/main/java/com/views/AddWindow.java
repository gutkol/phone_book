package com.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.listeners.IAddPersonListener;
import com.models.Person;

public class AddWindow extends JFrame {

	private List<IAddPersonListener> listeners = new ArrayList<>();
	
	private JPanel mainPanel;
	private GridBagConstraints constains;
	
	private JLabel firstNameLabel;
	private JLabel lastNameLabel;
	private JLabel addressLabel;
	private JLabel addressNumberLabel;
	private JLabel phoneNumberLabel;
	
	private JTextField firstNameTextField;
	private JTextField lastNameTextField;
	private JTextField addressTextField;
	private JTextField addressNumberTextField;
	private JTextField phoneNumberTextFiled;
	
	private JPanel buttonPanel;
	private JButton addButton;
	private JButton cancelButton;
	
	public AddWindow() {
		setTitle("Add");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		
		int width = 350;
	    int height = 400;
	    
		setSize(width, height);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screen.width - width) / 2;
		int y = (screen.height - height) / 2;
		setBounds(x, y, width, height);
		setSize(width, height);
		setLayout(new BorderLayout());
		
		Initialize();
		
		add(mainPanel, BorderLayout.CENTER);
		add(buttonPanel,BorderLayout.SOUTH);
	}
	
	public void Initialize(){
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridBagLayout());
		constains = new GridBagConstraints();
		constains.weightx = 1;
		constains.weighty = 1;
		
		firstNameLabel = new JLabel("First Name");
		lastNameLabel = new JLabel("Last name");
		addressLabel = new JLabel("Address");
		addressNumberLabel = new JLabel("Address Number");
		phoneNumberLabel = new JLabel("Phone Number");
		
		firstNameTextField = new JTextField();
		lastNameTextField = new JTextField();
		addressTextField = new JTextField();
		addressNumberTextField = new JTextField();
		phoneNumberTextFiled = new JTextField();
		
		addGridComponent(firstNameLabel, 0, 0, 1);
		addGridComponent(firstNameTextField, 0, 1, 2);
		addGridComponent(lastNameLabel, 0, 2, 1);
		addGridComponent(lastNameTextField, 0, 3, 2);
		addGridComponent(addressLabel, 0, 4, 1);
		addGridComponent(addressTextField, 0, 5, 2);
		addGridComponent(addressNumberLabel, 0, 6, 1);
		addGridComponent(addressNumberTextField, 0, 7, 2);
		addGridComponent(phoneNumberLabel, 0, 8, 1);
		addGridComponent(phoneNumberTextFiled, 0, 9, 2);
		
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new BorderLayout());
		addButton = new JButton("Add");
		addButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(isAnyEmpty()){
					JOptionPane.showMessageDialog(addButton,
						    "All field must be fullfield",
						    "Add",
						    JOptionPane.WARNING_MESSAGE);
					return;
				}
				if(!isPhoneNumberValid()){
					JOptionPane.showMessageDialog(addButton,
						    "Phone Number is not valid",
						    "Add",
						    JOptionPane.WARNING_MESSAGE);
					return;
				}
				
				addedActionPerformed();
				setVisible(false);
			}
		});
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		buttonPanel.add(addButton, BorderLayout.NORTH);
		buttonPanel.add(cancelButton,BorderLayout.SOUTH);
	}
	
	private void addGridComponent(Component component, int gridx, int gridy, int gridwidth){
		constains.fill = GridBagConstraints.HORIZONTAL;
		constains.gridx = gridx;
		constains.gridy = gridy;
		constains.gridwidth = gridwidth;
		constains.insets = new Insets(10, 10, 0, 10);
		mainPanel.add(component, constains);
	}
	
	private Boolean isAnyEmpty(){
		if(firstNameTextField.getText().equals("")){
			return true;
		}
		if(lastNameTextField.getText().equals("")){
			return true;
		}
		if(addressTextField.getText().equals("")){
			return true;
		}
		if(addressNumberTextField.getText().equals("")){
			return true;
		}
		if(phoneNumberTextFiled.getText().equals("")){
			return true;
		}
		return false;
	}
	
	private Boolean isPhoneNumberValid(){
		try{
			new Long(phoneNumberTextFiled.getText());
		}catch (Exception e) {
			return false;
		}
		
		return true;
	}
	
	public void addActionListener(IAddPersonListener listener){
		listeners.add(listener);
	}
	
	private void addedActionPerformed(){
		Person person = new Person(
				firstNameTextField.getText(), 
				lastNameTextField.getText(), 
				addressTextField.getText(), 
				addressNumberTextField.getText(), 
				new Long(phoneNumberTextFiled.getText()));
		
		for (IAddPersonListener listener : listeners) {
			listener.added(person);
		}
	}
}
