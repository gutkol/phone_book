package com.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SplashScreen extends JFrame {
	
	private int duration;

	  public SplashScreen(int d) {
	    duration = d;
	  }

	  public void showSplash() {
		setUndecorated(true);
	    JPanel content = (JPanel) getContentPane();
	    content.setBackground(Color.white);
	    
	    int width = 400;
	    int height = 400;
	    Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (screen.width - width) / 2;
	    int y = (screen.height - height) / 2;
	    setBounds(x, y, width, height);

	    JLabel label = new JLabel(new ImageIcon("src/main/resources/phone_book.jpg"));
	    JLabel copyrt = new JLabel("Copyright 2017, Rados�aw Nowak", JLabel.LEFT);
	    copyrt.setFont(new Font("Consolas", Font.ITALIC, 12));
	    
	    content.add(label, BorderLayout.CENTER);
	    content.add(copyrt, BorderLayout.SOUTH);
	    Color oraRed = new Color(156, 20, 20, 255);
	    content.setBorder(BorderFactory.createLineBorder(oraRed, 10));
	    setVisible(true);

	    try {
	      Thread.sleep(duration);
	    } catch (Exception e) {
	    }

	    setVisible(false);
	  }

	  public void showSplashAndExit() {
	    showSplash();
	  }

}
