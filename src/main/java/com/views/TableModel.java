package com.views;

import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

import org.springframework.stereotype.Component;

import com.models.Person;

@Component("tableModel")
public class TableModel extends AbstractTableModel {

	private ArrayList<Person> persons = new ArrayList<>();;
	private String[] columnNames = {
			"LP",
			"First Name", 
			"Last Name",
			"Steet Address",
			"Street Number",
			"Phone Number",
			"Is Selected"
	};
	
	public Person get(int index){
		return persons.get(index);
	}
	
	public ArrayList<Person> getSelected(){
		ArrayList<Person> selectedItems = new ArrayList<>();
		for (Person person : persons) {
			if(person.getIsSelected()){
				selectedItems.add(person);
			}
		}
		
		return selectedItems;
	}
	
	public ArrayList<Person> getAll(){
		return persons;
	}
	
	public void remove(int index){
		persons.remove(index);
		refresh();
	}
	
	public void remove(Person model) {
		persons.remove(model);
		refresh();
	}
	
	public void replaceData(ArrayList<Person> collection){
		persons = collection;
		refresh();
	}
	
	
	public void add(Person person){
		if(persons.stream().allMatch(x -> x.getId() != person.getId())){
			persons.add(person);
		}
		refresh();
	}
	
	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}
	
	@Override
	public int getColumnCount() {
		return 7;
	}

	@Override
	public int getRowCount() {
		if(persons != null){
			return persons.size();
		}
		return 0;
	}

	@Override
	public Object getValueAt(int row, int column) {
		Person person = persons.get(row);
		
		switch (column) {
		case 0:
			return row + 1;
		case 1:
			return person.getFirstName();
		case 2:
			return person.getLastName();
		case 3:
			return person.getStreet();
		case 4:
			return person.getStreetNumber();
		case 5:
			return person.getPhoneNumber();
		case 6:
			return person.getIsSelected();
		default:
			return null;
		}
	}
	
	@Override
	public void setValueAt(Object value, int row, int column) {
		if(column == 6){
			Person person = persons.get(row);
			person.setIsSelected((Boolean)value);
		}
		refresh();
    }
	
	 @Override
     public Class getColumnClass(int column) {
         switch (column) {
             case 0:
                 return Integer.class;
             case 1:
                 return String.class;
             case 2:
                 return String.class;
             case 3:
                 return String.class;
             case 4:
            	 return String.class;
             case 5:
            	 return Long.class;
             case 6:
            	 return Boolean.class;
             default:
                 return String.class;
         }
     }
	 
	 @Override
	 public boolean isCellEditable(int row, int column)
     { 
		 if(column == 6){
			 return true;
		 }
		 return false;
     }
	
	private void refresh(){
		fireTableDataChanged();
	}
}
