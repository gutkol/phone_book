package com.services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Service;

import com.models.Person;
import com.repository.IRepository;

@Service("searchService")
public class SearchService implements ISearchService{

	@Override
	public ArrayList<Person> search(String text, IRepository<Person> repository) {
		
		if(text.equals("")){
			return null;
		}
		
		ArrayList<Person> result = new ArrayList<>();
		Collection<Person> all = repository.Get();
		
		all.stream().filter((x) -> x.getFirstName().contains(text)
				|| x.getLastName().contains(text)
				|| x.getStreet().contains(text)
				|| ((Object)x.getPhoneNumber()).toString().contains(text))
				.forEach(x -> {
					result.add(x);
				});
		
		return result;
	}

}
