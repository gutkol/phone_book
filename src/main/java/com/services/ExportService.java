package com.services;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.springframework.stereotype.Service;

import com.models.PhoneBook;

@Service("exportService")
public class ExportService implements IExportService {

	public void exportToXml(String path, PhoneBook phoneBook) throws JAXBException {

		try {

			File file = new File(path);
			JAXBContext jaxbContext = JAXBContext.newInstance(PhoneBook.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(phoneBook, file);
			jaxbMarshaller.marshal(phoneBook, System.out);

		} catch (JAXBException e) {
			throw e;
		}
	}
}
