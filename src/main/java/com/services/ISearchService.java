package com.services;

import java.util.ArrayList;

import com.models.Person;
import com.repository.IRepository;

public interface ISearchService {

	ArrayList<Person> search(String text, IRepository<Person> repository);
}
