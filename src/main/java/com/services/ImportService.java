package com.services;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.stereotype.Service;

import com.models.PhoneBook;

@Service("importService")
public class ImportService implements IImportService {

	public PhoneBook importFromXml(String path) throws JAXBException {
		PhoneBook phoneBook = null;
		try {

			File file = new File(path);
			JAXBContext jaxbContext = JAXBContext.newInstance(PhoneBook.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			phoneBook = (PhoneBook) jaxbUnmarshaller.unmarshal(file);

		} catch (JAXBException e) {
			throw e;
		}

		return phoneBook;
	}
}
