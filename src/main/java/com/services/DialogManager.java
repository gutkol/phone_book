package com.services;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.springframework.stereotype.Service;

@Service("dialogManager")
public class DialogManager implements IDialogManager {
	final JFileChooser fileChooser = new JFileChooser();
	
	public DialogManager() {
		FileNameExtensionFilter xmlFilter = new FileNameExtensionFilter("xml files (*.xml)", "xml");
		fileChooser.addChoosableFileFilter(xmlFilter);
		fileChooser.setFileFilter(xmlFilter);
	}
	
	@Override
	public File showOpenDialog(Component component) {
		File file = null;
		int returnVal = fileChooser.showOpenDialog(component);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            file = fileChooser.getSelectedFile();    
        } 
        return file;
	}
	
	public String showSaveDialog(Component component){
		String path = null;
		int rVal = fileChooser.showSaveDialog(component);
		
	      if (rVal == JFileChooser.APPROVE_OPTION) {
	    	  String directoryPath = fileChooser.getCurrentDirectory().toString();
	    	  String fileName = fileChooser.getSelectedFile().getName();
	    	  if(!fileName.endsWith(".xml")){
	    		  fileName += ".xml";
	    	  }
	    	  path = directoryPath + "\\" + fileName;
	      }
	      return path;
	}
}
