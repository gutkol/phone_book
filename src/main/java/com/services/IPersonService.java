package com.services;

import java.io.File;

import javax.xml.bind.JAXBException;

import com.models.Person;
import com.views.TableModel;

public interface IPersonService {

	TableModel getTableModel();

	void getPersons();

	Boolean tryAdd(Person person);

	Boolean tryDelete(int id);

	void search(String text);

	void export(String path) throws JAXBException;

	void Import(File file) throws JAXBException;
}