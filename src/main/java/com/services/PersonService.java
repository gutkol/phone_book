package com.services;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.models.Person;
import com.models.PhoneBook;
import com.repository.IRepository;
import com.views.TableModel;

@Service("personService")
public class PersonService implements IPersonService {

	private TableModel tableModel;
	private IRepository<Person> repository;
	private ISearchService searchService;
	private IExportService exportService;
	private IImportService importService;
	private IValidator validator;
	
	@Autowired
	public PersonService(
			TableModel tableModel,
			IRepository<Person> repository,
			ISearchService searchService,
			IExportService exportService,
			IImportService importService,
			IValidator validator) {
		
		this.tableModel = tableModel;
		this.repository = repository;
		this.searchService = searchService;
		this.exportService = exportService;
		this.importService = importService;
		this.validator = validator;
	}
	
	@Override
	public TableModel getTableModel(){
		return tableModel;
	}
	
	@Override
	public void getPersons(){
		Collection<Person> persons = repository.Get();
		for (Person person : persons) {
			tableModel.add(person);
		}
	}

	@Override
	public Boolean tryAdd(Person person) {
		int id = repository.Add(person);
		Boolean wasSuccesful = id == 0;
		if(wasSuccesful){
			person.setId(id);
			tableModel.add(person);
		}
		
		return wasSuccesful;
	}

	@Override
	public Boolean tryDelete(int id) {
		Person person = tableModel.get(id);
		repository.Remove(person);
		tableModel.remove(person);
		return true;
	}

	@Override
	public void search(String text) {
		ArrayList<Person> searchedPersons = searchService.search(text, repository);
		if(searchedPersons != null){
			tableModel.replaceData(searchedPersons);
		}else{
			tableModel.replaceData(new ArrayList<>());
			getPersons();
		}
	}

	@Override
	public void export(String path) throws JAXBException {
		exportService.exportToXml(path, new PhoneBook(tableModel.getSelected()));
	}

	@Override
	public void Import(File file) throws JAXBException {
		PhoneBook phoneBook = importService.importFromXml(file.getAbsolutePath());
		ArrayList<Person> validPersons = validator.getDiff(tableModel.getAll(), phoneBook.getPersons());
		
		for (Person person : validPersons) {
			person.setIsSelected(false);
			tableModel.add(person);
			repository.Add(person);
		}	
	}
}
