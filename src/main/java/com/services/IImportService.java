package com.services;

import javax.xml.bind.JAXBException;

import com.models.PhoneBook;

public interface IImportService {

	PhoneBook importFromXml(String path) throws JAXBException;
}
