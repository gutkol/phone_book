package com.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service("validator")
public class Validator implements IValidator {

	@Override
	public <T> ArrayList<T> getDiff(ArrayList<T> existingCollection, ArrayList<T> newCollection) {
		ArrayList<T> diff = new ArrayList<>();
		for (T element : newCollection) {
			if(!existingCollection.stream().anyMatch(x -> x.equals(element))){
				diff.add(element);
			}
		}
		return diff;
	}
}
