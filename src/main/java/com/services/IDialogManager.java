package com.services;

import java.awt.Component;
import java.io.File;

public interface IDialogManager {

	File showOpenDialog(Component component);
	
	String showSaveDialog(Component component);
}
