package com.services;

import java.util.ArrayList;

public interface IValidator {

	<T> ArrayList<T> getDiff(ArrayList<T> existingCollection, ArrayList<T> newCollection);
}
