package com.services;

import javax.xml.bind.JAXBException;

import com.models.PhoneBook;

public interface IExportService {
	
	void exportToXml(String path, PhoneBook phoneBook) throws JAXBException;
}
