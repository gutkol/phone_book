package com.models;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PhoneBook {

	private ArrayList<Person> persons;
	
	public PhoneBook(){
		
	}
	
	public PhoneBook(ArrayList<Person> persons) {
		this.persons = persons;
	}

	public ArrayList<Person> getPersons() {
		return persons;
	}

	@XmlElementWrapper(name = "persons")
	public void setPersons(ArrayList<Person> persons) {
		this.persons = persons;
	}
}
