
import org.springframework.context.ApplicationContext;

import com.common.SpringBootstrap;
import com.views.MainWindow;
import com.views.PersonTablePanel;
import com.views.SplashScreen;

public class App {

	public static void main(String[] args) {
		
		ApplicationContext context = SpringBootstrap.buildContext();
		
		SplashScreen splash = new SplashScreen(3000);
	    splash.showSplashAndExit();
	    
	    MainWindow mainWindow = context.getBean("mainWindow", MainWindow.class);
		mainWindow.setVisible(true);
		
	}

}
